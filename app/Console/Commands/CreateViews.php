<?php

namespace App\Console\Commands;

use App\Aluno;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;

class CreateViews extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:view {modelname}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create your view automatically';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param
     * @return mixed
     */

    public function handle()
    {

        $path = "App".$this->argument('modelname');

        $file_path = "\app\Console\Commands\CreateViews.php";

        if (file_exists($file_path)) {

            $this->info("The file  exists");
        }else {

            $this->error("The file does  not exists");
            die;
        }


        $this->info($path);

        //Imprime message no console info() error()
        $this->info('Building your views !');

        //Pega o argumento enviado pelo user
        $this->argument('modelname');

        //return 'asdasdas';
    }
}
